# QingChen-Vue-JWT

#### 介绍
记录一下清晨平台的创建过程，包括前后端。

想自己开发一个项目，熟悉一下项目流程，打算从项目的整个流程开始分析，包括【设计】、【研发】、【测试】。

项目暂定包含几大模块，从用户使用角度：用户模块、博客模块、商品模块、交易模块。

其实就是基本实现多用户下的博客系统和商城系统。

#### 接口文档
    地址：https://www.apifox.cn/apidoc/shared-87bf9ed7-6bd6-4881-9d9b-d2500de223bb

#### 分支说明
project01 : 基本的框架，security登录功能实现，可看文档：[【清晨平台记录一】平台说明、思路+代码框架](https://blog.csdn.net/vaevaevae233/article/details/125258401)

project02 : 基于project01实现连接数据库实现用户登录，可看文档：[【清晨平台记录二】连接数据库实现用户登录](https://blog.csdn.net/vaevaevae233/article/details/125344971)

project03 : 基于project02实现平台后台的用户模块，包括权限等，这个是部分一，只实现了菜单、用户组、角色、用户模块的操作，可看文档：[【清晨平台记录三】代码实现平台后台用户模块一（包括权限相关）](https://blog.csdn.net/vaevaevae233/article/details/125346150)

project04 : 基于project03实现平台后台的用户模块，包括权限等，这个是部分二，追加实现了用户的个人信息的操作，可看文档：[【清晨平台记录四】代码实现平台后台用户模块二（主要实现个人账号的操作功能）](https://blog.csdn.net/vaevaevae233/article/details/125564015)

project05 : 基于project04实现平台后台的用户模块，包括权限等，这个是部分三，追加实现了登录账号信息、操作权限内容，可看文档：[【清晨平台记录五】代码实现平台后台用户模块三（主要实现登录账号信息、操作权限内容）](https://blog.csdn.net/vaevaevae233/article/details/125593920)

project06 : 基于project05实现平台后台的用户模块，包括权限等，这个是部分四，追加实现了用户的操作权限、数据权限内容，可看文档：[【清晨平台记录六】代码实现平台后台用户模块四（主要实现操作权限、数据权限的过滤）](https://blog.csdn.net/vaevaevae233/article/details/125651676)

